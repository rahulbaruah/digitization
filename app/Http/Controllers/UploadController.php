<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Upload;
use Storage;
use Log;

class UploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('upload.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $emp_no = $request->input('emp_no');
        
        $employee = Employee::where('emp_no', $emp_no)
                            ->first();
        
        if(!$employee) {
            return redirect()->back()->with(['msg' => ['Employee not found']]);
        }
        
        return view('upload.create')->with(['emp_no' => $emp_no, 'employee_name' => $employee->name]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'emp_no' => 'required',
			'file.*' => 'file',
		]);
		
		$emp_no = $request->input('emp_no');
		$tags = $request->input('tags');
		
		$employee_id = Employee::where('emp_no', $emp_no)
		                        ->first()->id;
		
		foreach($request->file('file') as $key=>$file) {
		    if($file){
				$path = $file->getRealPath();
				$originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
				$extension = strtolower($file->getClientOriginalExtension()); // get extension
				$fileName = snake_case($originalName.'_'.time().'.'.$extension); // renaming image
                
                $upload = new Upload;
		        $upload->employee_id = $employee_id;
		        $upload->filename = $fileName;
		        $upload->tags = $tags[$key];
		        $upload->save();
                
                $path = Storage::disk('servicebooks')->putFileAs(
                    $upload->id, $file, $fileName
                );
                
                $url = Storage::disk('servicebooks')->url($path);
                
                $upload->url = $url;
                $upload->save();
		    }
		}
		
		return redirect('/upload')->with(['msg' => ['Service book uploaded successfully...']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $upload = Upload::find($id);
        Storage::disk('servicebooks')->delete($upload->id.'/'.$upload->filename);
        Storage::disk('servicebooks')->deleteDirectory($upload->id);
        Upload::destroy($id);
    }
    
    public function search(){
        return view('upload.search');
    }
    
    public function searchresult(Request $request){
        $this->validate($request, [
			'emp_no' => 'required',
		]);
		
		$emp_no = $request->input('emp_no');
		$tags = $request->input('tags');
		
		$employee = Employee::where('emp_no', $emp_no)
		                    ->first();
		
		if(!$employee) {
            return redirect()->back()->with(['msg' => ['No employee found...']]);
        }
		
		$searchValues = preg_split('/\s+/', $tags, -1, PREG_SPLIT_NO_EMPTY);
		
		$upload = Upload::where('employee_id', $employee->id)
		                ->where(function ($q) use ($searchValues) {
                          foreach ($searchValues as $value) {
                            $q->orWhere('tags', 'like', "%{$value}%");
                          }
                        })
                        ->orderBy('created_at', 'desc')
                        ->get();
        
        if(!$upload->count()) {
            return redirect()->back()->with(['msg' => ['No results found...']]);
        }
        
        $data = ['upload' => $upload, 'employee' => $employee];
        
        return view('upload.searchresults')->with($data);
        
    }
    
    public function download($id)
    {
        $upload = Upload::find($id);
        return Storage::disk('servicebooks')->download($upload->id.'/'.$upload->filename);
    }
}
