<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employee';
    
    public function upload()
    {
        return $this->hasMany('App\Upload');
    }
}
