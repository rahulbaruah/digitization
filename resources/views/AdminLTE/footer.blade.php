<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Version 1.0.1
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="http://www.xcraft.co" target="_blank">Xcraft Online (P) Ltd</a>.</strong> All rights reserved.
</footer>