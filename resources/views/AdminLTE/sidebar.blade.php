<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <!-- Optionally, you can add icons to the links -->
        @if(Auth::user()->role == 'admin')
            <li class="treeview">
                        <a href="#"><i class="fa fa-user pull-left"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="/admin/users/create">Add New User</a></li>
                            <li><a href="/admin/users">View All Users</a></li>
                        </ul>
            </li>
        @endif
        <!--<li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
        <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>-->
        <li class="treeview">
          <a href="#"><i class="fa fa-user-circle"></i> <span>Employee</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/employee/create">New Employee</a></li>
            <li><a href="/employee">View All Employees</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-upload"></i> <span>Uploads</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/upload">Upload Servicebook</a></li>
            <li><a href="/servicebook/search">View Uploads</a></li>
          </ul>
        </li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>