@extends('AdminLTE.admin_template')

@section('page-title', 'Enter Employee Details')

@section('title', 'Enter Employee Details')

@section('content')
    <div class="row">
        <div class="col-md-8">
                  <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!--<div class="box-header with-border">
              <h3 class="box-title">Enter Servicemen Details</h3>
            </div>-->
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="/employee" method="POST">
                @csrf
              <div class="box-body">
                <div class="form-group">
                    <label for="emp-id" class="col-sm-2 control-label">Employee Id</label>

                    <div class="col-sm-10">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-vcard-o"></i>
                        </div>
                      <input name="emp_no" type="text" class="form-control" id="" placeholder="">
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-10">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-user-circle"></i>
                        </div>
                      <input name="name" type="text" class="form-control" id="" placeholder="">
                      </div>
                    </div>
                </div>
                <div class="form-group">
                  <label for="dob" class="col-sm-2 control-label">Date of Birth</label>

                  <div class="col-sm-10">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input name="dob" type="date" class="form-control">
                      </div>
                  </div>
                </div>
                <div class="form-group">
                    <label for="designation" class="col-sm-2 control-label">Designation</label>

                    <div class="col-sm-10">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-dot-circle-o"></i>
                        </div>
                          <input name="designation" type="text" class="form-control" id="" placeholder="">
                      </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="department" class="col-sm-2 control-label">Department</label>

                    <div class="col-sm-10">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-institution"></i>
                        </div>
                          <input name="department" type="text" class="form-control" id="" placeholder="">
                      </div>
                    </div>
                </div>

                <div class="form-group">
                  <label for="doa" class="col-sm-2 control-label">Date of Appointment</label>

                  <div class="col-sm-10">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input name="appointment_date" type="date" class="form-control" id="">
                      </div>
                  </div>
                </div>

              </div>   
            </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
    </div>
@endsection