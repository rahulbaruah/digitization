@extends('AdminLTE.admin_template')

@section('page-title', 'View Employees')

@section('title', 'View Employees')

@section('content')
<!-- message --->
@include('layouts.messages')
<div class="box">
	<div class="box-header">
        <!--<h3 class="box-title">Hover Data Table</h3>-->
    </div>
    <div class="box-body">
<div class="table-responsive">
<table id="allDataTable" class="table table-bordered table-hover" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Sl</th>
			<th>Employee Name</th>
			<th>Employee Id</th>
			<th>Date of Birth</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
@foreach ($employees as $key=>$employee)
		<tr>
			<td>{{ ($key+1) }}</td>
			<td>{{ $employee->name }}</td>
			<td>{{ $employee->emp_no }}</td>
			<td>{{ $employee->dob }}</td>
			<td>
				<a class="btn btn-default" href="{{ route('employee.edit', ['id' => $employee->id]) }}" role="button" title="Edit">
					<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
				</a>
				<a class="delBtn btn btn-danger" href="#" data-href="{{ route('employee.destroy', ['id' => $employee->id]) }}" role="button" title="Delete">
					<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
				</a>
			</td>
		</tr>
@endforeach
	</tbody>
</table>
</div>
</div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="/js/jquery.confirm.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click', '.delBtn', function(event){
			event.preventDefault();
			var url = $(this).data("href");
			$.confirm({
				text: "Confirm deletion",
			    confirm: function() {
			        $.ajax({
					    url: url,
					    headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
					    type: 'DELETE',
					    success: function(result) {
					        window.location.reload();
					    }
					});
			    },
			    cancel: function() {
			        // nothing to do
			    },
			    confirmButton: "Yes",
			    cancelButton: "No",
			    post: true,
			    confirmButtonClass: "btn-danger",
			    cancelButtonClass: "btn-default",
			    dialogClass: "modal-dialog modal-sm"
			});
		});

		var table = $('#allDataTable').DataTable({
    		"aaSorting": [[ 0, "desc" ]],
    	});

	});
</script>
@endsection