@extends('AdminLTE.admin_template')

@section('page-title', 'Search Service Books')

@section('title', 'Search Service Books')

@section('content')
<div class="box box-info">
<div class="box-body table-responsive">
              <table id="allDataTable" class="table table-hover table-bordered" cellspacing="0" width="100%">
                  <thead>
                <tr>
                  <th>File</th>
                  <th>Upload Date</th>
                  <th>Tags</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
    @foreach($upload as $key=>$file)
                <tr>
                  <td><a href="{{ asset($file->url) }}" target="_blank">{{ $file->filename }}</a></td>
                  <td>{{ $file->created_at }}</td>
                  <td>{{ $file->tags }}</td>
                  <td><a target="_blank" class="btn btn-primary" href="{{ url('/servicebook/download/'.$file->id) }}" role="button" title="Download">
					<span class="glyphicon glyphicon-download" aria-hidden="true"></span>
				</a> <a class="delBtn btn btn-danger" href="#" data-href="{{ route('upload.destroy', ['id' => $file->id]) }}" role="button" title="Delete">
					<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
				</a></td>
                </tr>
    @endforeach
    </tbody>
            </table>
</div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="/js/jquery.confirm.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click', '.delBtn', function(event){
			event.preventDefault();
			var url = $(this).data("href");
			$.confirm({
				text: "Confirm deletion",
			    confirm: function() {
			        $.ajax({
					    url: url,
					    headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
					    type: 'DELETE',
					    success: function(result) {
					        window.location.reload();
					    }
					});
			    },
			    cancel: function() {
			        // nothing to do
			    },
			    confirmButton: "Yes",
			    cancelButton: "No",
			    post: true,
			    confirmButtonClass: "btn-danger",
			    cancelButtonClass: "btn-default",
			    dialogClass: "modal-dialog modal-sm"
			});
		});

		var table = $('#allDataTable').DataTable();

	});
</script>
@endsection