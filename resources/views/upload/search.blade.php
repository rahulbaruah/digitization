@extends('AdminLTE.admin_template')

@section('page-title', 'Search Service Books')

@section('title', 'Search Service Books')

@section('content')
<form class="form-horizontal" action="/servicebook/search" method="post">
    @csrf
    <div class="row">
<!-- message --->
@include('layouts.messages')
    <!-- Main content -->
          <!-- Horizontal Form -->
        <div class="box box-info">
            <!--<div class="box-header with-border">
              <h3 class="box-title">View Service Book</h3>
            </div>-->
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <div class="form-group">
                    <label for="emp-id" class="col-sm-2 control-label">Employee Id</label>
                    <div class="col-md-5">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-vcard-o"></i>
                        </div>
                      <input name="emp_no" type="text" class="form-control" id="" placeholder="" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="emp-id" class="col-sm-2 control-label">Tag</label>
                    <div class="col-md-5">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-tags"></i>
                        </div>
                      <input name="tags" type="text" class="form-control" id="" placeholder="">
                      </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <!-- box-footer -->
            <div class="box-footer">
                <button type="submit" class="btn btn-info">Search</button>
            </div>
            <!-- /.box-footer -->
        </div>
    </div>
</form>
@endsection