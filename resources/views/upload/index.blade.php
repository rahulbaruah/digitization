@extends('AdminLTE.admin_template')

@section('page-title', 'Upload Service Book')

@section('title', 'Enter Employee Id')

@section('content')
<!-- message --->
@include('layouts.messages')
            <form method="POST" action="/upload/create">
            @csrf
            <div class="col-md-6">
            <div class="input-group">
              <input name="emp_no" type="text" class="form-control" placeholder="Employee Id">
              <span class="input-group-btn">
                <button class="btn btn-default" type="submit">Search</button>
              </span>
            </div><!-- /input-group -->
            </div>
          </form>
@endsection