@extends('AdminLTE.admin_template')

@section('page-title', 'Upload Service Books')

@section('title', 'Upload Service Books of '.$employee_name.' ('.$emp_no.')')

@section('css')
  @parent
<link rel="stylesheet" href="/css/selectize.bootstrap3.css" type="text/css" />
@endsection

@section('content')
<div class="row">
    <form action="/upload" method="post" class="form-horizontal" enctype="multipart/form-data">
      @csrf
      <input type="hidden" name="emp_no" value="{{ $emp_no }}"/>
    <!-- Main content -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!--<div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>-->
            <!-- /.box-header -->
            <!-- form start -->
            
              <div class="box-body" id="field-wrapper">
                <div class="form-group">
                  <label class="col-md-2 control-label">Upload Service Book</label>
                  <div class="row">
                    <div class="col-md-3">
                        <input type="file" name="file[]" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <input id="sel-0" type="text" name="tags[]" class="form-control" tabindex="-1" placeholder="Enter file tags">
                    </div>
                  </div>
                </div>
              </div>

              </div>   
        </div>
              <!-- /.box-body -->
        <div class="box-footer">
          <input type="hidden" name="MAX_FILE_SIZE" value="5000000">
          <button type="submit" class="btn btn-info">Submit</button>
          <button type="button" id="addMore" class="btn btn-primary pull-right">Add More</button>
        </div>
              <!-- /.box-footer -->
    </form>
</div>
@endsection

@section('js')
  @parent
<script type="text/javascript" src="/js/selectize.min.js"></script>
<script type="text/javascript">
  $('document').ready(function(){
    var i=1;
    $('#addMore').on('click', function(e){
      const field = '<div class="form-group"><label class="col-md-2 control-label">Upload Service Book</label><div class="row"><div class="col-md-3"><input type="file" name="file[]" class="form-control"></div><div class="col-md-6"><input id="sel-' + i + '" type="text" name="tags[]" class="form-control" tabindex="-1" placeholder="Enter file tags"></div></div></div>';
      $('#field-wrapper').append(field);
      selectize('#sel-' + i);
      i=i+1;
    });
    
    /*---------------tags--------------------------*/
	  selectize('#sel-0');
	  
	  function selectize(selector) {
	    $(selector).selectize({
    		delimiter: ',',
    		persist: false,
    		create: function(input) {
    			return {
    				value: input,
    				text: input
    			}
    		}
    	});
	  }
  	
  })
</script>
@endsection