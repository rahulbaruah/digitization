<!--errors-->
@if ($errors->any())
    <div class="alert alert-danger" role="alert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- //errors --->
<!-- messages --->
<?php if(session()->has('msg')){ $msg = array_flatten(session('msg')); } ?>
@if (!empty($msg))
<div class="alert alert-info alert-dismissible show" role="alert" style="{{ (empty($msg)) ? 'display:none' : '' }}">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    <ul>
    @foreach ($msg as $errmsg)
        <li>{{ $errmsg }}</li>
    @endforeach
    </ul>
</div>
@endif
<!-- //messages --->